import unittest
from main import bestimmung_runden_status


class testFunctions(unittest.TestCase):

    def test_bestimmung_runden_status(self):
        # Win
        self.assertEqual(bestimmung_runden_status(0, 2), 1)
        self.assertEqual(bestimmung_runden_status(1, 0), 1)
        self.assertEqual(bestimmung_runden_status(2, 1), 1)
        # Lose
        self.assertEqual(bestimmung_runden_status(0, 1), 2)
        self.assertEqual(bestimmung_runden_status(1, 2), 2)
        self.assertEqual(bestimmung_runden_status(2, 0), 2)
        # Draw
        self.assertEqual(bestimmung_runden_status(0, 0), 3)
        self.assertEqual(bestimmung_runden_status(1, 1), 3)
        self.assertEqual(bestimmung_runden_status(2, 2), 3)

if __name__ == '__main__':
    unittest.main()
