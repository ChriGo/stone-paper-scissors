import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import csv
import datetime
from random import *

def auswahl_optionen():
    # Spieler gibt gewaehlte Option ein / Computer waehlt zufaellig option
    korrekte_eingabe = 0
    while korrekte_eingabe == 0:
        auswahl_spieler = input('0 = Schere / 1 = Stein / 2 = Papier / 3 = SPIEL BEENDEN --- Deine Auswahl ist: ')
        if auswahl_spieler == "0" or auswahl_spieler == "1" or auswahl_spieler == "2" or auswahl_spieler == "3":
            auswahl_spieler = int(auswahl_spieler)
            korrekte_eingabe = 1
        else: print('Falsche eingabe! ')
    auswahl_computer = randint(0, 2)

    return auswahl_spieler, auswahl_computer

def bestimmung_runden_status(auswahl_spieler, auswahl_computer):
    # Vergleicht ausgewaehlte Optionen von Spieler und Computer und bestimmt Status der Runde
    if auswahl_spieler == 0 and auswahl_computer == 2 or \
            auswahl_spieler == 1 and auswahl_computer == 0 or \
            auswahl_spieler == 2 and auswahl_computer == 1:
        status_runde = 1

    elif auswahl_spieler == 0 and auswahl_computer == 1 or \
            auswahl_spieler == 1 and auswahl_computer == 2 or \
            auswahl_spieler == 2 and auswahl_computer == 0:
        status_runde = 2

    elif auswahl_spieler == 0 and auswahl_computer == 0 or \
            auswahl_spieler == 1 and auswahl_computer == 1 or \
            auswahl_spieler == 2 and auswahl_computer == 2:
        status_runde = 3

    return status_runde

def ausgabe_ergebnis(status_runde):
    # Abhaengig vom Status der Runde wird Spielausgang bestimmt
    if status_runde == 1:
        print('Gewonnen! ' + optionen[auswahl_spieler] + ' gewinnt gegen ' + optionen[auswahl_computer])
    elif status_runde == 2:
        print('Verloren! ' + optionen[auswahl_spieler] + ' verliert gegen ' + optionen[auswahl_computer])
    elif status_runde == 3:
        print('Unentschienden! ' + optionen[auswahl_spieler] + ' und ' + optionen[auswahl_computer])

def aktualisierung_zaehler(status_runde, spielstand, rundenanzahl, zaehler_spieler, zaehler_computer):
    # Zaehlt Spielstand und Rundenanzahl hoch
    if status_runde == 1:
        spielstand[0] = spielstand[0] + 1
        rundenanzahl += 1
    elif status_runde == 2:
        spielstand[1] = spielstand[1] + 1
        rundenanzahl += 1
    elif status_runde == 3:
        rundenanzahl += 1

    # Zaehlt gewaehlte Option des Spielers hoch
    if auswahl_spieler == 0:
        zaehler_spieler[0] += 1
    elif auswahl_spieler == 1:
        zaehler_spieler[1] += 1
    elif auswahl_spieler == 2:
        zaehler_spieler[2] += 1

    # Zaehlt gewaehlte Optionen des Computer hoch
    if auswahl_computer == 0:
        zaehler_computer[0] += 1
    elif auswahl_computer == 1:
        zaehler_computer[1] += 1
    elif auswahl_computer == 2:
        zaehler_computer[2] += 1

    return spielstand, rundenanzahl, zaehler_spieler, zaehler_computer


if __name__ == '__main__':
    # Initiale Werte

    optionen = ["Schere", "Stein", "Papier"]
    datum = datetime.date.today()
    spielstand = [0, 0]
    spielername = "Mr. Anonymous"
    rundenanzahl = 0
    zaehler_spieler = [0, 0, 0]  # Schere, Stein, Papier
    zaehler_computer = [0, 0, 0]
    teilnehmer = ["Spieler", "Computer"]  # Benoetigt zum Plotten
    # Veraenderbare Darstellungswerte
    darstellung_ergebnisse = False
    auswertung_df = False



    print('Lass uns eine Runde Schere Stein Papier spielen!\nIch spiele es immer mit diesen Optionen!\n ' + str(optionen))

    while True:

        auswahl_spieler, auswahl_computer = auswahl_optionen()

        if auswahl_spieler == 3:
            print('Der Endstand lautet: ' + str(spielstand) + "!")
            spielernamensabfrage = input('Dein Name lautet? ')
            if spielernamensabfrage != "":
                spielername = spielernamensabfrage
            print('Bye ' + spielername)
            break

        status_runde = bestimmung_runden_status(auswahl_spieler, auswahl_computer)
        ausgabe_ergebnis(status_runde)
        spielstand, rundenanzahl, zaehler_spieler, zaehler_computer = aktualisierung_zaehler(status_runde, spielstand,
                                                                                             rundenanzahl, zaehler_spieler,
                                                                                             zaehler_computer)

        #Ausgabe des aktuellen Spielstandes
        if auswahl_spieler == 1 or auswahl_spieler == 2 or auswahl_spieler == 3:
            print('Der Spielstand lautet: ' + str(spielstand) + "!")


    # Ergebnisse in csv
    with open('Spielstand.csv', 'a', newline='') as f:
        thewriter = csv.writer(f)

        thewriter.writerow([datum, spielername, spielstand[0], spielstand[1], rundenanzahl,
                            zaehler_spieler[0], zaehler_spieler[1], zaehler_spieler[2],
                            zaehler_computer[0], zaehler_computer[1], zaehler_computer[2]])

    if auswertung_df == True:
        filename = 'Spielstand.csv'
        df = pd.read_csv(filename, header=1)

        # Erhoeht die Anzahl angezeigter Spalten im Runwindow
        desired_width = 320
        pd.set_option('display.width', desired_width)
        np.set_printoptions(linewidth=desired_width)
        pd.set_option('display.max_columns', 20)

        # Pandas dataframe
        df['Winlooseratio'] = df['Wins'] / df['Looses']
        df['Winrate'] = df['Wins'] / df['Spielanzahl']

        anzahl_spiele_insgesamt = df['Spielanzahl'].sum()
        print('Gesamtanzahl Spiele betraegt: ' + str(anzahl_spiele_insgesamt))

        # Erstellung einer Spalte unter dem DF mit der Gesamten Anzahl Spiele und dessen Ausgabe als Text vor dem DF
        # df.loc['Gesamte Anzahl Spiele'] = pd.Series(df['Spielanzahl'].sum(), index=['Spielanzahl'])
        # print('Die Anzahl aller jemals gespielten Spiele betraegt: ' + str(df._get_value('Gesamte Anzahl Spiele', 'Spielanzahl')) + '!')


        print(df)

        # print(df.describe()) # Zusammenfassende Daten des df, schliesst NaN aus!

    if darstellung_ergebnisse == True:
        # Basic Plot
        plt.bar(teilnehmer, spielstand)
        plt.ylabel('Gewonnene Spiele')
        plt.xlabel('Teilnehmer')
        plt.title('Schere, Stein, Papier Ergebnisse')
        plt.show()